package me.javitb.privaliatest.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ResultEntity {

  @SerializedName("page") @Expose private Integer page;
  @SerializedName("results") @Expose private List<MovieEntity> results = null;
  @SerializedName("total_pages") @Expose private Integer totalPages;

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public List<MovieEntity> getResults() {
    return results;
  }

  public void setResults(List<MovieEntity> results) {
    this.results = results;
  }

  public Integer getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(Integer totalPages) {
    this.totalPages = totalPages;
  }
}
