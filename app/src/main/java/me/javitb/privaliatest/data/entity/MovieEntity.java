package me.javitb.privaliatest.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MovieEntity {

  @SerializedName("id") @Expose private Integer id;
  @SerializedName("title") @Expose private String title;
  @SerializedName("overview") @Expose private String overview;
  @SerializedName("poster_path") @Expose private String posterPath;

  public MovieEntity(Integer id, String title, String overview, String posterPath) {
    this.id = id;
    this.title = title;
    this.overview = overview;
    this.posterPath = posterPath;
  }

  public Integer getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getOverview() {
    return overview;
  }

  public String getPosterPath() {
    return posterPath;
  }
}
