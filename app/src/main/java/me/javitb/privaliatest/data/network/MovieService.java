package me.javitb.privaliatest.data.network;

import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import me.javitb.privaliatest.data.entity.ResultEntity;
import me.javitb.privaliatest.presentation.base.GenericCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public class MovieService {

  private MovieServiceApi movieServiceApi;
  private Gson gson;

  @Inject public MovieService(ServiceGenerator serviceGenerator, Gson gson) {
    this.movieServiceApi = serviceGenerator.createService(MovieServiceApi.class);
    this.gson = gson;
  }

  public void getPopularMovies(int page, final GenericCallback<ResultEntity> callback) {
    Map<String, String> queryMap = generatePopularListQueryMap(page);

    Call<ResultEntity> movies = this.movieServiceApi.getPopularMovies(queryMap);
    movies.enqueue(new Callback<ResultEntity>() {
      @Override public void onResponse(Call<ResultEntity> call, Response<ResultEntity> response) {
        callback.onSuccess(response.body());
      }

      @Override public void onFailure(Call<ResultEntity> call, Throwable t) {
        callback.onError();
      }
    });
  }

  private Map<String, String> generatePopularListQueryMap(int page) {
    Map<String, String> queryMap = new HashMap<>(3);

    queryMap.put("api_key", "93aea0c77bc168d8bbce3918cefefa45");
    queryMap.put("language", "en-US");
    queryMap.put("page", String.valueOf(page));

    return queryMap;
  }

  public void searchMovies(String searchText, int page,
      final GenericCallback<ResultEntity> callback) {
    Map<String, String> queryMap = generateSearchQueryMap(searchText, page);

    Call<ResultEntity> movies = this.movieServiceApi.searchMovies(queryMap);
    movies.enqueue(new Callback<ResultEntity>() {
      @Override public void onResponse(Call<ResultEntity> call, Response<ResultEntity> response) {
        callback.onSuccess(response.body());
      }

      @Override public void onFailure(Call<ResultEntity> call, Throwable t) {
        callback.onError();
      }
    });
  }

  private Map<String, String> generateSearchQueryMap(String searchText, int page) {
    Map<String, String> queryMap = new HashMap<>(3);

    queryMap.put("api_key", "93aea0c77bc168d8bbce3918cefefa45");
    queryMap.put("language", "en-US");
    queryMap.put("page", String.valueOf(page));
    queryMap.put("query", searchText);

    return queryMap;
  }

  interface MovieServiceApi {
    @GET("movie/popular") Call<ResultEntity> getPopularMovies(@QueryMap Map<String, String> query);

    @GET("search/movie") Call<ResultEntity> searchMovies(@QueryMap Map<String, String> query);
  }
}
