package me.javitb.privaliatest.data.entity.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import me.javitb.privaliatest.data.entity.MovieEntity;
import me.javitb.privaliatest.domain.model.Movie;

public class MovieEntityMapper {

  @Inject public MovieEntityMapper() {
  }

  public Movie transform(MovieEntity movieEntity) {
    Movie movie = new Movie();

    movie.setIdentifier(String.valueOf(movieEntity.getId()));
    movie.setTitle(movieEntity.getTitle());
    movie.setOverview(movieEntity.getOverview());
    movie.setUrlImage(movieEntity.getPosterPath());

    return movie;
  }

  public List<Movie> transformList(List<MovieEntity> movieEntityList) {

    List<Movie> movieList = new ArrayList<>(movieEntityList.size());

    if (!movieEntityList.isEmpty()) {
      for (MovieEntity movieEntity : movieEntityList) {
        Movie movie = transform(movieEntity);
        movieList.add(movie);
      }
    }

    return movieList;
  }
}
