package me.javitb.privaliatest.data.repository;

import javax.inject.Inject;
import me.javitb.privaliatest.data.entity.ResultEntity;
import me.javitb.privaliatest.data.entity.mapper.MovieEntityMapper;
import me.javitb.privaliatest.data.network.MovieService;
import me.javitb.privaliatest.domain.MovieRepository;
import me.javitb.privaliatest.domain.model.Result;
import me.javitb.privaliatest.presentation.base.GenericCallback;

public class MovieRepositoryData implements MovieRepository {

  private MovieService movieService;
  private MovieEntityMapper movieEntityMapper;

  @Inject
  public MovieRepositoryData(MovieService movieService, MovieEntityMapper movieEntityMapper) {
    this.movieService = movieService;
    this.movieEntityMapper = movieEntityMapper;
  }

  @Override public void getPopularMovies(int page, final GenericCallback<Result> callback) {
    this.movieService.getPopularMovies(page, new GenericCallback<ResultEntity>() {
      @Override public void onSuccess(ResultEntity result) {
        Result res = new Result();
        res.setPage(result.getPage());
        res.setMovies(movieEntityMapper.transformList(result.getResults()));
        res.setTotalPages(result.getTotalPages());
        // TODO: mapper and send back info
        callback.onSuccess(res);
      }

      @Override public void onError() {
        callback.onError();
      }
    });
  }

  @Override
  public void searchMovies(String searchText, int page, final GenericCallback<Result> callback) {
    this.movieService.searchMovies(searchText, page, new GenericCallback<ResultEntity>() {
      @Override public void onSuccess(ResultEntity result) {
        Result res = new Result();
        res.setPage(result.getPage());
        res.setMovies(movieEntityMapper.transformList(result.getResults()));
        res.setTotalPages(result.getTotalPages());

        callback.onSuccess(res);
      }

      @Override public void onError() {
        callback.onError();
      }
    });
  }
}
