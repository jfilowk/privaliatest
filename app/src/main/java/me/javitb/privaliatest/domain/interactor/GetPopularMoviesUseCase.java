package me.javitb.privaliatest.domain.interactor;

import me.javitb.privaliatest.domain.Interactor;
import me.javitb.privaliatest.domain.model.Result;
import me.javitb.privaliatest.presentation.base.GenericCallback;

public interface GetPopularMoviesUseCase extends Interactor {

  void execute(int numberPage, GenericCallback<Result> callback);
}
