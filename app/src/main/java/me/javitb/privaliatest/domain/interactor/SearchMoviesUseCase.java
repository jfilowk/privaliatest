package me.javitb.privaliatest.domain.interactor;

import me.javitb.privaliatest.domain.Interactor;
import me.javitb.privaliatest.domain.model.Result;
import me.javitb.privaliatest.presentation.base.GenericCallback;

public interface SearchMoviesUseCase extends Interactor {

  void execute(String searchText, int page, GenericCallback<Result> callback);
}
