package me.javitb.privaliatest.domain;

public interface Interactor extends Runnable {

  @Override void run();
}
