package me.javitb.privaliatest.domain.interactor;

import java.util.concurrent.ExecutorService;
import javax.inject.Inject;
import me.javitb.privaliatest.domain.MainThread;
import me.javitb.privaliatest.domain.MovieRepository;
import me.javitb.privaliatest.domain.model.Result;
import me.javitb.privaliatest.presentation.base.GenericCallback;

public class SearchMoviesUseCaseImpl implements SearchMoviesUseCase {

  private MovieRepository movieRepository;
  private ExecutorService executor;
  private MainThread mainThread;

  private int page;
  private String searchText;
  private GenericCallback<Result> callback;

  @Inject public SearchMoviesUseCaseImpl(MovieRepository movieRepository, ExecutorService executor,
      MainThread mainThread) {
    this.movieRepository = movieRepository;
    this.executor = executor;
    this.mainThread = mainThread;
  }

  @Override public void execute(String searchText, int page, GenericCallback<Result> callback) {
    if (callback == null) {
      throw new IllegalArgumentException("Not null");
    }

    this.searchText = searchText;
    this.page = page;
    this.callback = callback;

    this.executor.submit(this);
  }

  @Override public void run() {
    this.movieRepository.searchMovies(searchText, page, new GenericCallback<Result>() {
      @Override public void onSuccess(Result result) {
        notifySearchMovies(result);
      }

      @Override public void onError() {
        notifyOnError();
      }
    });
  }

  private void notifySearchMovies(final Result result) {
    this.mainThread.post(new Runnable() {
      @Override public void run() {
        callback.onSuccess(result);
      }
    });
  }

  private void notifyOnError() {
    this.mainThread.post(new Runnable() {
      @Override public void run() {
        callback.onError();
      }
    });
  }
}
