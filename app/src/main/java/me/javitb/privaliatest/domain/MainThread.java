package me.javitb.privaliatest.domain;

public interface MainThread {

  void post(Runnable runnable);
}
