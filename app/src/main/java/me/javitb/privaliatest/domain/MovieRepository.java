package me.javitb.privaliatest.domain;

import me.javitb.privaliatest.domain.model.Result;
import me.javitb.privaliatest.presentation.base.GenericCallback;

public interface MovieRepository {

  void getPopularMovies(int page, GenericCallback<Result> callback);

  void searchMovies(String search, int page, GenericCallback<Result> callback);
}
