package me.javitb.privaliatest.domain.interactor;

import java.util.concurrent.ExecutorService;
import javax.inject.Inject;
import me.javitb.privaliatest.domain.MainThread;
import me.javitb.privaliatest.domain.MovieRepository;
import me.javitb.privaliatest.domain.model.Result;
import me.javitb.privaliatest.presentation.base.GenericCallback;

public class GetPopularMoviesUseCaseImpl implements GetPopularMoviesUseCase {

  private MovieRepository movieRepository;
  private ExecutorService executor;
  private MainThread mainThread;

  private int page;
  private GenericCallback<Result> callback;

  @Inject public GetPopularMoviesUseCaseImpl(MovieRepository movieRepository,
      ExecutorService executorService, MainThread mainThread) {
    this.movieRepository = movieRepository;
    this.executor = executorService;
    this.mainThread = mainThread;
  }

  @Override public void execute(int numberPage, final GenericCallback<Result> callback) {
    if (callback == null) {
      throw new IllegalArgumentException("No null");
    }

    this.page = numberPage;
    this.callback = callback;

    this.executor.submit(this);
  }

  @Override public void run() {
    this.movieRepository.getPopularMovies(page, new GenericCallback<Result>() {
      @Override public void onSuccess(Result result) {
        notifyResultMovies(result);
      }

      @Override public void onError() {

      }
    });
  }

  private void notifyResultMovies(final Result result) {
    this.mainThread.post(new Runnable() {
      @Override public void run() {
        callback.onSuccess(result);
      }
    });
  }

  private void notifyOnError() {
    this.mainThread.post(new Runnable() {
      @Override public void run() {
        callback.onError();
      }
    });
  }
}
