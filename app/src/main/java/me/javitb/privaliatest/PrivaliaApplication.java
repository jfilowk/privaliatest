package me.javitb.privaliatest;

import android.app.Application;
import me.javitb.privaliatest.presentation.internal.di.component.ApplicationComponent;
import me.javitb.privaliatest.presentation.internal.di.component.DaggerApplicationComponent;
import me.javitb.privaliatest.presentation.internal.di.module.ApplicationModule;

public class PrivaliaApplication extends Application {

  private ApplicationComponent applicationComponent;

  @Override public void onCreate() {
    super.onCreate();

    initApplicationComponent();
  }

  private void initApplicationComponent() {
    if (this.applicationComponent == null) {
      this.applicationComponent = DaggerApplicationComponent.builder()
          .applicationModule(new ApplicationModule(this))
          .build();
    }

    this.applicationComponent.inject(this);
  }

  public ApplicationComponent getApplicationComponent() {
    return applicationComponent;
  }
}
