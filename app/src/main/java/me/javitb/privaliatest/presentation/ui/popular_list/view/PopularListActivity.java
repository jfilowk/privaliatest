package me.javitb.privaliatest.presentation.ui.popular_list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import butterknife.BindView;
import java.util.List;
import javax.inject.Inject;
import me.javitb.privaliatest.R;
import me.javitb.privaliatest.presentation.base.BaseActivity;
import me.javitb.privaliatest.presentation.internal.di.component.DaggerPopularListComponent;
import me.javitb.privaliatest.presentation.internal.di.component.PopularListComponent;
import me.javitb.privaliatest.presentation.model.MovieModel;
import me.javitb.privaliatest.presentation.ui.popular_list.adapter.EndlessRecyclerViewScrollListener;
import me.javitb.privaliatest.presentation.ui.popular_list.adapter.ListAdapter;
import me.javitb.privaliatest.presentation.ui.popular_list.presenter.PopularListPresenter;
import me.javitb.privaliatest.presentation.ui.search.view.SearchActivity;

public class PopularListActivity extends BaseActivity
    implements PopularListPresenter.PopularListView {

  @BindView(R.id.recycler_view) RecyclerView movieRecyclerView;

  @Inject PopularListPresenter presenter;

  private PopularListComponent component;

  private ListAdapter adapter;
  private EndlessRecyclerViewScrollListener scrollListener;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    initializeDagger();
    initializePresenter();
    initializeAdapter();
    initializeRecyclerView();

    presenter.loadElements(1);
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main_menu, menu);

    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_goto_search:
        presenter.clickSearch();

      default:
    }
    return true;
  }

  private void initializeRecyclerView() {
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    movieRecyclerView.setLayoutManager(layoutManager);
    movieRecyclerView.setAdapter(adapter);
    scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
      @Override public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
        presenter.loadElements(page);
      }
    };
    movieRecyclerView.addOnScrollListener(scrollListener);
  }

  private void initializeAdapter() {
    adapter = new ListAdapter();
  }

  private void initializeDagger() {
    if (this.component == null) {
      this.component = DaggerPopularListComponent.builder()
          .applicationComponent(getApplicationComponent())
          .activityModule(getActivityModule())
          .build();
    }

    this.component.inject(this);
  }

  private void initializePresenter() {
    presenter.attachView(this);
  }

  @Override public int getLayoutId() {
    return R.layout.activity_popular_list;
  }

  @Override public void displayMovies(List<MovieModel> movieModelList) {
    adapter.addAll(movieModelList);
    adapter.notifyDataSetChanged();
  }

  @Override public void goToSearch() {
    Intent intent = new Intent(this, SearchActivity.class);
    startActivity(intent);
  }

  @Override public void showLoading() {
    Toast.makeText(this, "Loading movies...", Toast.LENGTH_SHORT).show();
  }

  @Override public void showError() {

  }
}
