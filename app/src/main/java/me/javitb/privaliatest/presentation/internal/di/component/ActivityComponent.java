package me.javitb.privaliatest.presentation.internal.di.component;

import android.app.Activity;
import dagger.Component;
import me.javitb.privaliatest.presentation.base.BaseActivity;
import me.javitb.privaliatest.presentation.internal.di.PerActivity;
import me.javitb.privaliatest.presentation.internal.di.module.ActivityModule;

@PerActivity
@Component(dependencies = { ApplicationComponent.class }, modules = { ActivityModule.class })
public interface ActivityComponent {

  void inject(BaseActivity baseActivity);

  Activity activity();
}
