package me.javitb.privaliatest.presentation.ui.popular_list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import me.javitb.privaliatest.R;
import me.javitb.privaliatest.presentation.model.MovieModel;

public class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private final List<MovieModel> movieList;

  public ListAdapter() {
    movieList = new ArrayList<MovieModel>();
  }

  public void addAll(List<MovieModel> movieList) {
    this.movieList.addAll(movieList);
  }

  public void removeAll() {
    this.movieList.clear();
  }

  @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list, parent, false);

    return new PopularMovieViewHolder(view);
  }

  @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    PopularMovieViewHolder viewHolder = (PopularMovieViewHolder) holder;
    MovieModel movie = movieList.get(position);
    viewHolder.bindMovie(movie);
  }

  @Override public int getItemCount() {
    return movieList.size();
  }

  class PopularMovieViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_movie_poster) ImageView posterImageView;
    @BindView(R.id.tv_movie_title) TextView titleTextView;
    @BindView(R.id.tv_movie_overview) TextView overviTextView;

    PopularMovieViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }

    void bindMovie(MovieModel movie) {

      titleTextView.setText(movie.getTitle());
      overviTextView.setText(movie.getOverview());

      Picasso.with(itemView.getContext())
          .load(movie.getUrlImage())
          .fit()
          .centerCrop()
          .into(posterImageView);
    }
  }
}
