package me.javitb.privaliatest.presentation.model;

public class MovieModel {

  private String identifier;
  private String title;
  private String overview;
  private String urlImage;

  public MovieModel() {
  }

  public MovieModel(String identifier, String title, String overview, String urlImage) {
    this.identifier = identifier;
    this.title = title;
    this.overview = overview;
    this.urlImage = urlImage;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getOverview() {
    return overview;
  }

  public void setOverview(String overview) {
    this.overview = overview;
  }

  public String getUrlImage() {
    return urlImage;
  }

  public void setUrlImage(String urlImage) {
    this.urlImage = urlImage;
  }

  @Override public String toString() {
    return "MovieModel{" +
        "identifier='" + identifier + '\'' +
        ", title='" + title + '\'' +
        ", overview='" + overview + '\'' +
        ", urlImage='" + urlImage + '\'' +
        '}';
  }
}
