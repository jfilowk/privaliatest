package me.javitb.privaliatest.presentation.ui.search.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import butterknife.BindView;
import java.util.List;
import javax.inject.Inject;
import me.javitb.privaliatest.R;
import me.javitb.privaliatest.presentation.base.BaseActivity;
import me.javitb.privaliatest.presentation.internal.di.component.DaggerSearchComponent;
import me.javitb.privaliatest.presentation.internal.di.component.SearchComponent;
import me.javitb.privaliatest.presentation.model.MovieModel;
import me.javitb.privaliatest.presentation.ui.popular_list.adapter.EndlessRecyclerViewScrollListener;
import me.javitb.privaliatest.presentation.ui.popular_list.adapter.ListAdapter;
import me.javitb.privaliatest.presentation.ui.search.presenter.SearchPresenter;

public class SearchActivity extends BaseActivity implements SearchPresenter.SearchView {

  @BindView(R.id.rv_search) RecyclerView searchRecyclerView;

  @Inject SearchPresenter presenter;

  private SearchComponent component;

  private ListAdapter adapter;
  private EndlessRecyclerViewScrollListener scrollListener;

  private String searchText;
  private boolean submit = false;
  private Handler handlerQuery;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    initializeDagger();
    initializePresenter();
    initializeAdapter();
    initializeRecyclerView();

    customizeToolbar();
  }

  private void initializeDagger() {
    if (component == null) {
      component =
          DaggerSearchComponent.builder().applicationComponent(getApplicationComponent()).build();
    }
    component.inject(this);
  }

  private void initializePresenter() {
    presenter.attachView(this);
  }

  private void initializeAdapter() {
    this.adapter = new ListAdapter();
  }

  private void initializeRecyclerView() {
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    searchRecyclerView.setLayoutManager(layoutManager);
    searchRecyclerView.setAdapter(adapter);

    scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
      @Override public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
        presenter.search(searchText, page + 1);
      }
    };

    searchRecyclerView.addOnScrollListener(scrollListener);
  }

  private void customizeToolbar() {
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setTitle("Search");
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.search_menu, menu);
    handlerQuery = new Handler();

    final MenuItem searchItem = customizeMenuItem(menu);

    final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

    searchView.setQueryHint("Search movie!");
    searchView.setIconified(false);

    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override public boolean onQueryTextSubmit(String query) {
        submit = true;

        if (getSupportActionBar() != null) {
          getSupportActionBar().setDisplayShowTitleEnabled(true);
          getSupportActionBar().setTitle(query);
        }

        hideSearchView(searchView);

        return false;
      }

      @Override public boolean onQueryTextChange(String newText) {
        if (submit) {
          submit = false;
          return false;
        }

        searchText = newText;

        handlerQuery.removeCallbacksAndMessages(null);
        handlerQuery.postDelayed(new Runnable() {
          @Override public void run() {
            if (searchView.getQuery().length() != 0) {
              clearSearchRecyclerView();
              presenter.search(searchText, 1);
            } else if (searchView.getQuery().length() == 0) {
              clearSearchRecyclerView();
              searchText = "";
            }
          }
        }, 300);

        return false;
      }
    });

    return true;
  }

  private void clearSearchRecyclerView() {
    adapter.removeAll();
    adapter.notifyDataSetChanged();
    scrollListener.resetState();
  }

  private void hideSearchView(SearchView searchView) {
    searchView.setQuery("", false);
    searchView.setIconified(true);
  }

  @NonNull private MenuItem customizeMenuItem(Menu menu) {
    final MenuItem searchItem = menu.findItem(R.id.action_search);
    searchItem.expandActionView();
    MenuItemCompat.setOnActionExpandListener(searchItem,
        new MenuItemCompat.OnActionExpandListener() {
          @Override public boolean onMenuItemActionExpand(MenuItem item) {
            return false;
          }

          @Override public boolean onMenuItemActionCollapse(MenuItem item) {
            finish();
            return false;
          }
        });
    return searchItem;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
    }
    return true;
  }

  @Override public int getLayoutId() {
    return R.layout.activity_search;
  }

  @Override public void displayResult(List<MovieModel> movieModelList) {
    adapter.addAll(movieModelList);
    adapter.notifyDataSetChanged();
  }

  @Override public void showLoading() {
    Toast.makeText(this, "Loading movies...", Toast.LENGTH_SHORT).show();
  }

  @Override public void showError() {

  }
}
