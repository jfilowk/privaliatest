package me.javitb.privaliatest.presentation.internal.di;

public interface HasComponent<C> {
  C getComponent();
}
