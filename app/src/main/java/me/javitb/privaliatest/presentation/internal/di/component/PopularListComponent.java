package me.javitb.privaliatest.presentation.internal.di.component;

import dagger.Component;
import me.javitb.privaliatest.presentation.internal.di.PerActivity;
import me.javitb.privaliatest.presentation.internal.di.module.ActivityModule;
import me.javitb.privaliatest.presentation.internal.di.module.PopularListModule;
import me.javitb.privaliatest.presentation.ui.popular_list.view.PopularListActivity;

@PerActivity @Component(dependencies = ApplicationComponent.class, modules = {
    ActivityModule.class, PopularListModule.class
}) public interface PopularListComponent {

  void inject(PopularListActivity activity);
}
