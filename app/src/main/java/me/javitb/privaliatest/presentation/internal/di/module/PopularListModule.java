package me.javitb.privaliatest.presentation.internal.di.module;

import dagger.Module;
import dagger.Provides;
import me.javitb.privaliatest.presentation.internal.di.PerActivity;
import me.javitb.privaliatest.presentation.ui.popular_list.presenter.PopularListPresenter;
import me.javitb.privaliatest.presentation.ui.popular_list.presenter.PopularListPresenterImpl;

@Module public class PopularListModule {

  @Provides @PerActivity PopularListPresenter providePopularListPresenter(
      PopularListPresenterImpl popularListPresenter) {
    return popularListPresenter;
  }
}
