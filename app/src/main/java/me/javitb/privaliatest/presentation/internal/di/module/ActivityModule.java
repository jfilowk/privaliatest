package me.javitb.privaliatest.presentation.internal.di.module;

import android.app.Activity;
import dagger.Module;
import dagger.Provides;
import me.javitb.privaliatest.presentation.base.BaseActivity;
import me.javitb.privaliatest.presentation.internal.di.PerActivity;

@Module public class ActivityModule {

  private BaseActivity baseActivity;

  public ActivityModule(BaseActivity baseActivity) {
    this.baseActivity = baseActivity;
  }

  @Provides @PerActivity Activity provideActivity() {
    return baseActivity;
  }
}
