package me.javitb.privaliatest.presentation.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.javitb.privaliatest.PrivaliaApplication;
import me.javitb.privaliatest.R;
import me.javitb.privaliatest.presentation.internal.di.component.ApplicationComponent;
import me.javitb.privaliatest.presentation.internal.di.module.ActivityModule;

public abstract class BaseActivity extends AppCompatActivity {

  @BindView(R.id.toolbar) Toolbar toolbar;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(getLayoutId());
    initializeButterKnife();
    initializeToolbar();
  }

  public abstract int getLayoutId();

  public ApplicationComponent getApplicationComponent() {
    return ((PrivaliaApplication) getApplication()).getApplicationComponent();
  }

  public ActivityModule getActivityModule() {
    return new ActivityModule(this);
  }

  private void initializeButterKnife() {
    ButterKnife.bind(this);
  }

  private void initializeToolbar() {
    if (toolbar != null) {
      setSupportActionBar(toolbar);
    }
  }
}
