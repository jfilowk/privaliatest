package me.javitb.privaliatest.presentation.internal.di.module;

import dagger.Module;
import dagger.Provides;
import me.javitb.privaliatest.presentation.internal.di.PerActivity;
import me.javitb.privaliatest.presentation.ui.search.presenter.SearchPresenter;
import me.javitb.privaliatest.presentation.ui.search.presenter.SearchPresenterImpl;

@Module public class SearchModule {

  @Provides @PerActivity SearchPresenter provideSearchPresenter(
      SearchPresenterImpl searchPresenter) {
    return searchPresenter;
  }
}
