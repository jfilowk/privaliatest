package me.javitb.privaliatest.presentation.base;

public interface GenericCallback<T> {

  void onSuccess(T result);

  void onError();
}
