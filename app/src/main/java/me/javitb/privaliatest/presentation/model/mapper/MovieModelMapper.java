package me.javitb.privaliatest.presentation.model.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import me.javitb.privaliatest.domain.model.Movie;
import me.javitb.privaliatest.presentation.model.MovieModel;

public class MovieModelMapper {

  @Inject public MovieModelMapper() {
  }

  public MovieModel tranform(Movie movie) {
    MovieModel movieModel = new MovieModel();

    movieModel.setIdentifier(movie.getIdentifier());
    movieModel.setTitle(movie.getTitle());
    movieModel.setOverview(movie.getOverview());
    movieModel.setUrlImage("https://image.tmdb.org/t/p/w500" + movie.getUrlImage());

    return movieModel;
  }

  public List<MovieModel> transformList(List<Movie> movieList) {

    List<MovieModel> movieModelList = new ArrayList<>(movieList.size());

    if (!movieList.isEmpty()) {
      for (Movie movie : movieList) {
        MovieModel movieModel = tranform(movie);
        movieModelList.add(movieModel);
      }
    }

    return movieModelList;
  }
}
