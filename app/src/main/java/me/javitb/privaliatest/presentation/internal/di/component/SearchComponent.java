package me.javitb.privaliatest.presentation.internal.di.component;

import dagger.Component;
import me.javitb.privaliatest.presentation.internal.di.PerActivity;
import me.javitb.privaliatest.presentation.internal.di.module.ActivityModule;
import me.javitb.privaliatest.presentation.internal.di.module.SearchModule;
import me.javitb.privaliatest.presentation.ui.search.view.SearchActivity;

@PerActivity @Component(dependencies = ApplicationComponent.class, modules = {
    ActivityModule.class, SearchModule.class
}) public interface SearchComponent {

  void inject(SearchActivity activity);
}
