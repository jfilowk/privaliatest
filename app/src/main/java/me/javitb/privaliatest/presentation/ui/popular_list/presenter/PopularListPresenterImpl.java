package me.javitb.privaliatest.presentation.ui.popular_list.presenter;

import java.util.List;
import javax.inject.Inject;
import me.javitb.privaliatest.domain.interactor.GetPopularMoviesUseCase;
import me.javitb.privaliatest.domain.model.Result;
import me.javitb.privaliatest.presentation.base.GenericCallback;
import me.javitb.privaliatest.presentation.model.MovieModel;
import me.javitb.privaliatest.presentation.model.mapper.MovieModelMapper;

public class PopularListPresenterImpl implements PopularListPresenter {

  private GetPopularMoviesUseCase popularMoviesUseCase;
  private MovieModelMapper movieModelMapper;

  private PopularListView view;

  @Inject public PopularListPresenterImpl(GetPopularMoviesUseCase popularMoviesUseCase,
      MovieModelMapper movieModelMapper) {
    this.popularMoviesUseCase = popularMoviesUseCase;
    this.movieModelMapper = movieModelMapper;
  }

  @Override public void loadElements(int page) {
    view.showLoading();

    this.popularMoviesUseCase.execute(page, new GenericCallback<Result>() {
      @Override public void onSuccess(Result result) {
        view.showLoading();
        displayMovies(movieModelMapper.transformList(result.getMovies()));
      }

      @Override public void onError() {
        displayError();
      }
    });
  }

  @Override public void clickSearch() {
    view.goToSearch();
  }

  private void displayMovies(List<MovieModel> result) {
    view.displayMovies(result);
  }

  private void displayError() {
    view.showError();
  }

  @Override public void attachView(PopularListView view) {
    this.view = view;
  }
}
