package me.javitb.privaliatest.presentation.ui.popular_list.presenter;

import java.util.List;
import me.javitb.privaliatest.presentation.model.MovieModel;

public interface PopularListPresenter {

  void loadElements(int page);

  void clickSearch();

  void attachView(PopularListView view);

  interface PopularListView {

    void displayMovies(List<MovieModel> movieModelList);

    void goToSearch();

    void showLoading();

    void showError();
  }
}
