package me.javitb.privaliatest.presentation.ui.search.presenter;

import java.util.List;
import me.javitb.privaliatest.presentation.model.MovieModel;

public interface SearchPresenter {

  void search(String textSearch, int page);

  void attachView(SearchView view);

  interface SearchView {
    void displayResult(List<MovieModel> movieModelList);

    void showLoading();

    void showError();
  }
}
