package me.javitb.privaliatest.presentation.ui.search.presenter;

import java.util.List;
import javax.inject.Inject;
import me.javitb.privaliatest.domain.interactor.SearchMoviesUseCase;
import me.javitb.privaliatest.domain.model.Result;
import me.javitb.privaliatest.presentation.base.GenericCallback;
import me.javitb.privaliatest.presentation.model.MovieModel;
import me.javitb.privaliatest.presentation.model.mapper.MovieModelMapper;

public class SearchPresenterImpl implements SearchPresenter {

  private SearchMoviesUseCase searchMoviesUseCase;
  private MovieModelMapper movieModelMapper;

  private SearchView view;

  @Inject public SearchPresenterImpl(SearchMoviesUseCase searchMoviesUseCase,
      MovieModelMapper movieModelMapper) {
    this.searchMoviesUseCase = searchMoviesUseCase;
    this.movieModelMapper = movieModelMapper;
  }

  @Override public void search(final String searchText, int page) {
    view.showLoading();

    this.searchMoviesUseCase.execute(searchText, page, new GenericCallback<Result>() {
      @Override public void onSuccess(Result result) {
        List<MovieModel> movieModelList = movieModelMapper.transformList(result.getMovies());
        view.displayResult(movieModelList);
      }

      @Override public void onError() {
        view.showError();
      }
    });
  }

  @Override public void attachView(SearchView view) {
    this.view = view;
  }
}
