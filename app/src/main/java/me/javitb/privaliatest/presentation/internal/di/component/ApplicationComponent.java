package me.javitb.privaliatest.presentation.internal.di.component;

import android.content.Context;
import dagger.Component;
import javax.inject.Singleton;
import me.javitb.privaliatest.PrivaliaApplication;
import me.javitb.privaliatest.domain.MainThread;
import me.javitb.privaliatest.domain.interactor.GetPopularMoviesUseCase;
import me.javitb.privaliatest.domain.interactor.SearchMoviesUseCase;
import me.javitb.privaliatest.presentation.internal.di.module.ApplicationModule;

@Singleton @Component(modules = ApplicationModule.class) public interface ApplicationComponent {

  void inject(PrivaliaApplication application);

  Context applicationContext();

  MainThread mainThread();

  GetPopularMoviesUseCase getPopularMoviesUseCase();

  SearchMoviesUseCase searchMoviesUseCase();
}
