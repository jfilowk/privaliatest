package me.javitb.privaliatest.presentation.internal.di.module;

import android.content.Context;
import com.google.gson.Gson;
import dagger.Module;
import dagger.Provides;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.inject.Singleton;
import me.javitb.privaliatest.PrivaliaApplication;
import me.javitb.privaliatest.data.entity.mapper.MovieEntityMapper;
import me.javitb.privaliatest.data.network.MovieService;
import me.javitb.privaliatest.data.network.ServiceGenerator;
import me.javitb.privaliatest.data.repository.MovieRepositoryData;
import me.javitb.privaliatest.domain.MainThread;
import me.javitb.privaliatest.domain.MainThreadImpl;
import me.javitb.privaliatest.domain.MovieRepository;
import me.javitb.privaliatest.domain.interactor.GetPopularMoviesUseCase;
import me.javitb.privaliatest.domain.interactor.GetPopularMoviesUseCaseImpl;
import me.javitb.privaliatest.domain.interactor.SearchMoviesUseCase;
import me.javitb.privaliatest.domain.interactor.SearchMoviesUseCaseImpl;
import me.javitb.privaliatest.presentation.model.mapper.MovieModelMapper;

@Module public class ApplicationModule {

  private PrivaliaApplication application;

  public ApplicationModule(PrivaliaApplication application) {
    this.application = application;
  }

  @Provides @Singleton Context provideApplicationContext() {
    return application.getApplicationContext();
  }

  @Provides @Singleton Gson provideGson() {
    return new Gson();
  }

  @Provides @Singleton MainThread provideMainThread(MainThreadImpl mainThread) {
    return mainThread;
  }

  @Provides @Singleton ExecutorService provideExecutorService() {
    return Executors.newFixedThreadPool(4);
  }

  @Provides @Singleton ServiceGenerator provideServiceGenerator() {
    return new ServiceGenerator("https://api.themoviedb.org/3/");
  }

  @Provides @Singleton MovieModelMapper provideMovieModelMapper() {
    return new MovieModelMapper();
  }

  @Provides @Singleton MovieEntityMapper provideMovieEntityMapper() {
    return new MovieEntityMapper();
  }

  @Provides @Singleton MovieService provideMovieService(ServiceGenerator serviceGenerator,
      Gson gson) {
    return new MovieService(serviceGenerator, gson);
  }

  @Provides @Singleton MovieRepository provideMovieRepository(MovieRepositoryData movieRepository) {
    return movieRepository;
  }

  @Provides @Singleton GetPopularMoviesUseCase provideGetPopularMoviesUseCase(
      GetPopularMoviesUseCaseImpl getPopularMoviesUseCase) {
    return getPopularMoviesUseCase;
  }

  @Provides @Singleton SearchMoviesUseCase provideSearchMoviesUseCase(
      SearchMoviesUseCaseImpl searchMoviesUseCase) {
    return searchMoviesUseCase;
  }
}
