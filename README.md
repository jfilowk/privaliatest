La prueba la he realizado siguiendo una arquitectura Clean, diferenciando capas de presentation, domain y data.

Para la capa de presentation he utilizado MVP. He utilizado RecyclerView y para la búsqueda un SearchView en el Toolbar.

En la lógica de negocio he creado dos casos de uso. La obtención de las películas más populares y otro caso de uso para la búsqueda. He hecho uso del Executor framework.

En la parte de data, he creado un repositorio. En este caso solo tiene el datasource de la API. Para las request HTTP he utilizado Retrofit2 con GSON parseando las respuestas.

Librerías utilizadas
Retrofit2
GSON
Picasso
ButterKnife
Dagger2